import { HttpStatus, Injectable } from '@nestjs/common';
import { Response } from './models/response.models';

@Injectable()
export class AppService {
  getHello()/*: string*/ {
    return new Response("All Good", HttpStatus.OK, "Hello World!");
    //return 'Hello World!';
  }
}
