import { Module } from '@nestjs/common';
import { RandomsService } from './randoms.service';
import { RandomsController } from './randoms.controller';

@Module({
  providers: [RandomsService],
  controllers: [RandomsController],
  exports: [RandomsService],
})
export class RandomsModule {}
