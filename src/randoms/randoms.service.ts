import { HttpStatus, Injectable } from '@nestjs/common';
import { Response } from '../models/response.models';

@Injectable()
export class RandomsService {
    randomString() {
        const string =
          Math.random().toString(36).substring(2, 15) +
          Math.random().toString(36).substring(2, 15);
        return new Response("All Good", HttpStatus.OK, string);
      }
}
