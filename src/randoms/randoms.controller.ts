import { Controller, Get } from '@nestjs/common';
import { RandomsService } from './randoms.service';
import { Response } from '../models/response.models';

@Controller('randoms')
export class RandomsController {
    constructor(private readonly randomsService: RandomsService) {}

    @Get('string')
    randomString(): Response {
      return this.randomsService.randomString();
    }
}
