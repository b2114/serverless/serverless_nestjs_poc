import { ConflictException, Injectable, InternalServerErrorException, Logger } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { User, UserDocument} from "./user.schema";
import { Model } from 'mongoose';

export const mongoDuplicatedEntriesErrorCode = 11000;


@Injectable()
export class UserRepository {
  constructor(@InjectModel(User.name) private readonly userModel: Model<UserDocument>) {}
  private logger = new Logger(UserRepository.name);

  async create(newUser: User): Promise<User> {
      const user = new this.userModel(newUser);
      try {
          const savedUser = await user.save();
          this.logger.log(`Successfully createp new user ${newUser.email}`);
          return savedUser;
        } catch (error) {
          if (error.code === mongoDuplicatedEntriesErrorCode) {
            throw new ConflictException(`User "${newUser.email}" already exists`, newUser.email);
          } else {
            this.logger.error(
              `Failed to create new user. Data: ${JSON.stringify(newUser)}`,
              error.stack,
            );
            throw new InternalServerErrorException(error);
          }
      }
  }

  async getAll(): Promise<User[]> {
    try {
      return this.userModel.find();
    } catch (error) {
        throw new InternalServerErrorException(error);
    }
  }

  getTest(): string{
    return "HEY HEY HEY"
  }
}