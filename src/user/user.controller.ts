import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UserService } from './user.service';
import { Response } from '../models/response.models';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService){}

    @Post("/")
    createUser(@Body() createUserDto: CreateUserDto): Promise<Response>{
      return this.userService.createUser(createUserDto);
    }

    @Get("/")
    gatAllUsers(): Promise<Response> {
      return this.userService.getAllUsers();
    }

    @Get("/hey")
    gatTest(): Response {
      return this.userService.getTest();
    }
}
