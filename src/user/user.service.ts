import { HttpStatus, Injectable } from '@nestjs/common';
import { Response } from '../models/response.models';
import { CreateUserDto } from './dto/create-user.dto';
import { UserRepository } from './user.repository';
import { User } from './user.schema';

@Injectable()
export class UserService {
    constructor(private readonly userRepository: UserRepository) {}

    async createUser(createUserDto: CreateUserDto) {
        const user: User = await this.userRepository.create(createUserDto)
        return new Response("All Good", HttpStatus.OK, user);
    }

    async getAllUsers() {
        const users: User[] = await this.userRepository.getAll()
        return new Response("All Good", HttpStatus.OK, users);
    }

    getTest() {
        const test: string = this.userRepository.getTest()
        return new Response("All Good", HttpStatus.OK, test);
    }
}
