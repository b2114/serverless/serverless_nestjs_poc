# Serverless POC

## Netlify - hosting

- Hosting platform for static sites
- Active in the jamstack ecosystem
- Fast and easy way to host static site

The process is to build our code and store the result of that process in a `./dist` folder, and upload those files to netlify

This process can be automatic if netlify is connected to github repo with the source code.

## Netlify - Serverless functions

- Instead of setting up the whole server, with this strategy we are not responsability to set the server and maintain the server, we just deploy functions that will run on a server.
- Allow us to add dynamic features without need to setup the server
- In frontend they are framework agnostic


But, how do we add backend functionality?

1º

Add a `./functions` directory (default) with all your files inside, then create and export handlers.

![handlers](images/1.png)

2º

Add `netlify.toml` file with netlify configurations:

![netlify config](images/2.png)

- `[build] functions` - Specify the directory where are the functions;
- `[redirect]` - This will allow frontend call only `/api/something...` and will redirect to the default `localhost:port/.netlify/functions/<something...-function-file-name>`

3º

Instal netlify dependencies: `yarn add netlify-cli`

4º

`netlify dev` command looks for the type of the project and will recognize that is a `REACT/VUE/NEXT/GATSBY/...` project and:

- runs the serverless functions(backend) (stored in `./functions` directory);
- runs the frontend;
- netlify dev sets the proxy automatically between frontend and backend ports, so frontend code can have make a relative path call to `/api/...`, if netlify redirect rule is set
- You can then access `localhost:port` and is running like if was deployed

5º

NOTES:

- If we need environment variables that are store in netlify we can login local to netlify and run `netlify link`, then once we run `netlify dev` the environment variables needed will be fetched from netlify.

